# -*- mode:org; coding: utf-8 -*-

#+TITLE:     task for swap
#+AUTHOR:    Wensheng Xie
#+EMAIL:     wxie@member.fsf.org
#+LANGUAGE:  en
#+OPTIONS: H:2 num:nil toc:nil \n:nil @:t ::t |:t ^:{} _:{} *:t TeX:t LaTeX:t
#+STYLE: <link rel="stylesheet" type="text/css" href="org.css" />
#+LATEX_CLASS: report
#+LATEX_CLASS_OPTIONS: [a4paper]
#+ATTR_LATEX: width=0.38\textwidth wrap placement={r}{0.4\textwidth}
#+ATTR_LATEX: :float multicolumn
#+REVEAL_TRANS: None
#+REVEAL_THEME: Black
#+TAGS: @work(w) @home(h) @road(r) laptop(l) pc(p) { @read : @read_book @read_ebook }
#+ATTR_ORG: :width 30
#+ATTR_HTML: width="100px"
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+STARTUP: fold

* tasks for swap
Swap is virtual memory located on your hard disk and extends the RAM when the
memory utilization is high.
There a swap space and swap file in GNU/Linux system.
Swap space is a physical disk partition with no file. Swap file is a file in
file system used as swap.
Swap space is usually created during installation. If you have no swap space,
it's recommended to create a swap file.

** check swap configuration
#+BEGIN_SRC sh
swapon  # list swap
swapon --show
free -h # list swap
df -h   # check / for free spacw
#+END_SRC
After installation, exFAT drivers will be mounted automatically. You can use the
command to format drivers to exfat:
#+BEGIN_SRC sh
mkfs.exfat
#+END_SRC

** create swap file
*** using tools ~fallocate~
#+BEGIN_SRC sh
sudo fallocate -l 1G /swap_file # swap_file name can be others
#+END_SRC

*** using tools ~dd~
#+BEGIN_SRC sh
sudo dd if=/dev/zero of=/swap_file bs=1024 count=1048576
#+END_SRC

*** swap file permission
#+BEGIN_SRC sh
sudo chmod 600  /swap_file  # 600 is only for swap file
#+END_SRC

*** setup swap area and activate it
#+BEGIN_SRC sh
sudo mkswap /swap_file
sudo swapon /swap_file
#+END_SRC

*** make swap file persistent
#+BEGIN_SRC sh
sudo vim /etc/fstab
#+END_SRC
add the following line in ~fstab~
#+BEGIN_SRC
/swap_file swap swap defaults 0 0
#+END_SRC

** create swap partition
*** check disk space
#+BEGIN_SRC sh
fdisk --list
free -m
swapon -s
#+END_SRC
*** using ~fdisk~ to create a swap partition
#+BEGIN_SRC sh
fdisk /dev/sdb # enter fdisk command line
# create a new partition
n
p
default (1)
default (2048)
default (1048575)
# define the new partition as swap
t
82 # linux swap type
w  # save
q  # exit fdisk command line
# format the partition as swap
mkswap /dev/sdb1
# enable the swap
swapon /dev/sdb1
free -m # check swap
#+END_SRC
*** add to ~/etc/fstab~ file
#+BEGIN_SRC
/dev/sdb1 swap swap defaults 0 0
#+END_SRC
*** Create swap partition for lvm
#+BEGIN_SRC sh
lvcreate rootvg -n swapvol -L 8G # 8G logical volume
mkswap /dev/rootvg/swapvol       # format to swap
swapon -v /dev/rootvg/swapvol    # enable
#+END_SRC
*** extend swap for lvm
#+BEGIN_SRC sh
swapoff -v /dev/rootvg/swapvol   # turn off swap
lvm lvresize /dev/rootvg/swapvol -L +8G  # add 8G
mkswap /dev/rootvg/swapvol       # format
swapon -va
#+END_SRC
*** make swap persistent
edit ~/etc/fstab~ file, add
#+BEGIN_SRC
/dev/rootvg/swapvol swap swap defaults 0 0
#+END_SRC
** setup ~swappiness~
#+BEGIN_SRC sh
cat /proc/sys/vm/swappiness  # default value = 60
sudo sysctl vm.swappiness=20 # set lower for production
#+END_SRC

You can make it persitent in file ~/etc/sysctl.conf~
#+BEGIN_SRC
vm.swappiness=20
#+END_SRC
** remove swap partition
#+BEGIN_SRC sh
swapoff -v /dev/rootvg/swapvol  # disable swap
lvremove /dev/rootvg/swapvol    # remove the volume
# remove the /etc/fstab file entry for swap
#+END_SRC
