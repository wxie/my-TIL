# -*- mode:org; coding: utf-8 -*-

#+TITLE:     task for working on quicklisp
#+AUTHOR:    Wensheng Xie
#+EMAIL:     wxie@member.fsf.org
#+LANGUAGE:  en
#+OPTIONS: H:2 num:nil toc:nil \n:nil @:t ::t |:t ^:{} _:{} *:t TeX:t LaTeX:t
#+STYLE: <link rel="stylesheet" type="text/css" href="org.css" />
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [a4paper]
#+ATTR_LATEX: width=0.38\textwidth wrap placement={r}{0.4\textwidth}
#+ATTR_LATEX: :float multicolumn
#+REVEAL_TRANS: None
#+REVEAL_THEME: Black
#+TAGS: @work(w) @home(h) @road(r) laptop(l) pc(p) { @read : @read_book @read_ebook }
#+ATTR_ORG: :width 30
#+ATTR_HTML: width="100px"
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+STARTUP: fold

* tasks for quicklisp
** [2021]
*** [2021-02]
**** [2021-02-16 Tue]
***** install quicklisp [5/5]
 - [X] source [[https://www.quicklisp.org][quicklisp]]
 - [X] [[https://beta.quicklisp.org/quicklisp.lisp.asc][quicklisp PGP key]] and [[https://beta.quicklisp.org/release-key.txt][quicklisp release sign key]]
 - [X] download quicklisp.lisp and verify
 #+begin_src shell
curl -O https://beta.quicklisp.org/quicklisp.lisp
curl -O https://beta.quicklisp.org/quicklisp.lisp.asc
curl -O https://beta.quicklisp.org/release-key.txt
gpg --import release-key.txt
gpg --verify quicklisp.lisp.asc quicklisp.lisp
 #+end_src
 - [X] load
 #+begin_src shell
sbcl --load quicklisp.lisp
 #+end_src
 - [X] install
 #+begin_src lisp
(quicklisp-quickstart:install)
 #+end_src
***** use quicklist [3/3]
  - [X] general usage
  #+begin_src lisp
;   To load a system, use:
(ql:quickload "system-name")
;   To find systems, use:
(ql:system-apropos "term")
;   To remove software, use:
(ql:uninstall system-name)
  #+end_src
  - [X] To load Quicklisp every time you start Lisp, use:
  #+begin_src lisp
(ql:add-to-init-file)
  #+end_src
  - [X] update quicklisp
  #+begin_src lisp
; To get updated software, use:
(ql:update-dist "quicklisp")
; To update the Quicklisp client, use:
(ql:update-client)
  #+end_src
***** quicklisp init file: #P"/Users/quicklisp//.sbclrc"
      #+begin_src lisp
;;; The following lines added by ql:add-to-init-file:
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

;; To get updated software, use:
(ql:update-all-dists)

;; To update the Quicklisp client, use:
(ql:update-client)
      #+end_src
**** [2021-07-24 六]
***** install SLIME
#+BEGIN_SRC lisp
;;; To install and configure SLIME, use:
(ql:quickload "quicklisp-slime-helper")
#+END_SRC
** local project
*** easy way
#+BEGIN_SRC shell
cd ~/quicklisp/local-projects/
git clone git://github.com/xach/format-time.git
(ql:quickload "format-time")
#+END_SRC
*** standard way
#+BEGIN_SRC lisp
(push #p"/projects/my-project/" asdf:*central-registry*)
(ql:quickload "my-project")
#+END_SRC
