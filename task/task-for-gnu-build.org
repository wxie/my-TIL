# -*- mode:org; coding: utf-8 -*-

#+TITLE:     task for GNU build system
#+AUTHOR:    Wensheng Xie
#+EMAIL:     wxie@member.fsf.org
#+LANGUAGE:  en
#+OPTIONS: H:2 num:nil toc:nil \n:nil @:t ::t |:t ^:{} _:{} *:t TeX:t LaTeX:t
#+STYLE: <link rel="stylesheet" type="text/css" href="org.css" />
#+LATEX_CLASS: report
#+LATEX_CLASS_OPTIONS: [a4paper]
#+ATTR_LATEX: width=0.38\textwidth wrap placement={r}{0.4\textwidth}
#+ATTR_LATEX: :float multicolumn
#+REVEAL_TRANS: None
#+REVEAL_THEME: Black
#+TAGS: @work(w) @home(h) @road(r) laptop(l) pc(p) { @read : @read_book @read_ebook }
#+ATTR_ORG: :width 30
#+ATTR_HTML: width="100px"
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+STARTUP: fold

* tasks for GNU build system

** GNU make
*** make basics
**** make for SparkStudio
***** [2020-05-28 四]
****** DONE test new release 2.1.3 [3/3]
       DEADLINE: <2020-05-28 四>
  - [X] add 'ls' test for SparkStudio
  - [X] upgrade local SparkStudio version
  - [X] update sparksource.stdout to 2.1.3
**** GNU make basics
***** [2020-06-01 一]
****** DONE GNU make format: Makefile
       CLOSED: [2020-06-01 一 11:01]
 #+BEGIN_SRC makefile
# comment
target (file to be created): prerequisities (file depended)
# commands must use TAB not spaces
	command1; \
	continue-command1;
	command2;
 #+END_SRC
****** DONE define variables in Makefile
       CLOSED: [2020-06-01 一 11:05]
 #+BEGIN_SRC makefile
VAR=--abc --help --verbose

target1: need-file1
	command1 $(VAR)
	command2 ${VAR}
 #+END_SRC
****** DONE automatic variable in Makefile
       CLOSED: [2020-06-01 一 11:07]
     $@    the file name of the target
     $<    the name of the first prerequisite (i.e., dependency)
     $^    the names of all prerequisites (i.e., dependencies)
     $(@D)    the directory part of the target
     $(@F)    the file part of the target
     $(<D)    the directory part of the first prerequisite (i.e., dependency)
     $(<F)    the file part of the first prerequisite (i.e., dependency)
****** DONE Pattern rules
       CLOSED: [2020-06-01 一 11:10]
 #+BEGIN_SRC makefile
# use the symbol % as a wildcard, to be expanded to any string of text
R_OPTS=--vanilla

Figs/%.pdf: R/%.R
    cd $(<D);R CMD BATCH $(R_OPTS) $(<F)
 #+END_SRC
**** Makefile tips
***** [2020-07-02 四]
****** DONE list all targets on a Makefile [4/4]
       CLOSED: [2020-07-02 四 21:10]
       - CLOSING NOTE [2020-07-02 四 21:10] \\
         try it for sparksource
  - [X] read the original page
    [[https://diamantidis.github.io/tips/2020/07/01/list-makefile-targets][all targets]]
  - [X] default make target
 #+BEGIN_SRC makefile
# the default target of `make` is `all`
# so when is do `make`, it runs `make all`
# change it to `help`
.DEFAULT_GOAL := help
.PHONY: help

help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
	| sed -n 's/^\(.*\): \(.*\)##\(.*\)/\1\3/p' \
	| column -t  -s ' '
 #+END_SRC
  - [X] add comment for each target as
 #+BEGIN_SRC makefile
install: ## Install
	@echo "Installing..."

run: ## Run
	@echo "Running..."
 #+END_SRC
  - [X] the final result is
 #+BEGIN_SRC shell
make
#output:
# install Install
# run Run
 #+END_SRC
****** other examples of auto Makefile documentation
******* example 1
 #+BEGIN_SRC makefile
.PHONY: help

help:
	.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "%-30s %s\n", $$1, $$2}'
 #+END_SRC
******* example 2
 #+BEGIN_SRC makefile
.PHONY: help

help: # Show this help screen
	@ack '^[a-zA-Z_-]+:.*?# .*$$' $(MAKEFILE_LIST) |\ sort -k1,1 |\ awk 'BEGIN {FS = ":.*?# "}; {printf "\033[1m%-30s\033[0m %s\n", $$1, $$2}'
 #+END_SRC
******* example 3
 #+BEGIN_SRC makefile
.PHONY: help

help: # Display this help message
	@awk 'BEGIN { \ FS = ": #"; \ printf "App name\n\n"; \ printf "Usage:\n make \033[38;5;141m<target>\033[0m\n\nTargets:\n" \ } /^(.+)\: #\ (.+)/ { \ printf " \033[38;5;141m%-11s\033[0m %s\n", $$1, $$2 \ }' $(MAKEFILE_LIST)
 #+END_SRC
*** a simple Makefile
 #+BEGIN_SRC makefile
# default target is 'help'
.PHONY: help

help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
	| sed -n 's/^\(.*\): \(.*\)##\(.*\)/\1\3/p' \
	| column -t  -s ' '

# makefile variables
CC=gcc
CFLAGS=`pkg-config --cflags --libs guile-3.0`
DEPS =
SIMPLE_OBJ = simple-guile.o

%.o: %.c $(DEPS) ## Build-objects
	$(CC) -c -o $@ $< $(CFLAGS)

simple-guile: $(SIMPLE_OBJ) ## Build-final-executable
	$(CC) -o $@ $^ $(CFLAGS)

clean: ## Cleaning-files
	rm -f *.o *~ core
 #+END_SRC
** [[https://thoughtbot.com/blog/the-magic-behind-configure-make-make-install][The magic behind configure, make, make install]]

*** installed software from source
#+BEGIN_SRC shell
./configure
make
sudo make install
#+END_SRC

**** Configure the software: ./configure
     It makes sure all of the dependencies for the rest of the build and install
     process are available, and finds out whatever it needs to know to use those
     dependencies.

     Makefile.in -> ./configure -> Makefile

**** Build the software: make
     This runs a series of tasks defined in a Makefile to build the finished
     program from its source code.

     Makefile -> make -> files to run, such as bin

**** Install the software: make install
     This copy the built program, and its libraries and documentation, to the
     correct locations.

     The program’s binary will be copied to a directory on your PATH, the
     program’s manual page will be copied to a directory on your MANPATH, and
     any other files it depends on will be safely stored in the appropriate
     place.

     Makefile -> make install -> copy files to, e.g. //usr/bin/

*** autotools
**** minimum project structure
     - src: for source files
     - must have: NEWS README AUTHORS ChangLog
     - optional:  GNU COPYING TODO INSTALL README-alpha THANKS
     - automake --add-missing: compile config.guess config.rpath config.sub texinfo.tex
     - configure.ac must have: AC_INIT AC_OUTPUT m4/
#+BEGIN_SRC configure
AC_INIT([penguin], [2019.3.6], [[seth@example.com][8]])
AC_OUTPUT                     # no parameter
AM_INIT_AUTOMAKE              # no parameter
AC_CONFIG_FILES([file...])    # input file=file.in output=file
AC_PROG_CXX                   # build tool C++
#+END_SRC

     - Makefile.am same as Makefile
#+BEGIN_SRC makefile
bin_PROGRAMS = penguin          # install penguin in bindir
penguin_SOURCES = penguin.cpp   # penguin source files in src/
bin_SCRIPTS = bin/penguin       # no compile scripts in bindir
AUTOMAKE_OPTIONS = foreign subdir-objects  # source files other than src/

clean-local:  # good for development
        @rm config.status configure config.log
        @rm Makefile
        @rm -r autom4te.cache/
        @rm aclocal.m4
        @rm compile install-sh missing Makefile.in
#+END_SRC

**** autoconf <- configure.ac
     configure.ac file written in m4sh—a combination of m4 macros and POSIX
     shell script—to describe what the configure script needs to do.

#+BEGIN_SRC sample: configure.ac
# program name=helloworld; version=0.1; maintainer=wxie@email
AC_INIT([helloworld], [0.1], [wxie@email])

# use automake
AM_INIT_AUTOMAKE

# use c compiler, otherwise use AC_PATH_PROG
AC_PROG_CC

# use Makefile.in
AC_CONFIG_FILES([Makefile])

# output the script
AC_OUTPUT
#+END_SRC

**** automake <- Makefile.am
     automake will use Makefile.am to generated the Makefile.in file.

#+BEGIN_SRC sample: Makefile.am
# not following the standard layout of a GNU project
AUTOMAKE_OPTIONS = foreign

# build a program called helloworld, use bindir
bin_PROGRAMS = helloworld

# find the source file
helloworld_SOURCES = main.c
#+END_SRC

**** run autotools and generate configure script and Makefile.in template
#+BEGIN_SRC shell
aclocal # Set up an m4 environment
./bootstrap # a shell script to regenerate configure, Makefile.in, INSTALL files.
autoconf # Generate configure from configure.ac
automake --add-missing # Generate Makefile.in from Makefile.am
./configure # Generate Makefile from Makefile.in
make distcheck # Use Makefile to build and test a tarball to distribute
#+END_SRC

**** Uniform Naming Scheme of automake

***** [[https://www.gnu.org/prep/standards/standards.html#Directory-Variables][GNU standard]]
#+BEGIN_SRC autoconf
@srcdir@='inserted by the configure shell script'

@prefix@=/usr/local
@exec_prefix@=$(prefix)
@bindir@=$(exec_prefix)/bin
@sbindir@=$(exec_prefix)/sbin

@datarootdir@=$(prefix)/share
@datadir@=$(datarootdir)

@includedir@=$(prefix)/include

@docdir@=$(datarootdir)/doc/$(PACKAGE)
@infodir@=$(datarootdir)/info

@mandir@=$(datarootdir)/man
@man1dir@=$(mandir)/man1
#+END_SRC

***** Automake extension
#+BEGIN_SRC Makefile
pkgdatadir=$(datadir)/$(PACKAGE)
pkgincludedir=$(includedir)/$(PACKAGE)
#+END_SRC

***** dist_ directory
#+BEGIN_SRC autoconf
dist_doc_DATA = doc/fdl.texi
dist_man1_MANS = doc/ballandpaddle.man

nobase_dist_pkgdata_DATA = images/vortex.pgm sounds/whirl.ogg

imagesdir = $(pkgdatadir)/images
soundsdir = $(pkgdatadir)/sounds
dist_images_DATA = images/vortex.pgm
dist_sounds_DATA = sounds/whirl.ogg
#+END_SRC

**** installation directory
     - prefix: ac_default_prefix=/usr/local
     - bindir: ac_default=${prefix}/bin
     - datarootdir: read-only data root directory=${prefix}/share
     - datadir: read-only data directory=datarootdir
     - pkgdatadir: $(datadir)/$(PACKAGE)
     - docdir: installed doc=$(datadir)/doc
     - mandir: dist_man_MANS=$(datadir)/man
     - infodir: info_TEXTINFOS=$(datadir)/info

** Setting up a GNU Guile project with Autotools
[[http://www.draketo.de/proj/with-guise-and-guile/guile-projects-with-autotools.html][http://www.draketo.de/proj/with-guise-and-guile/guile-projects-with-autotools.html]]
These projects follow the traditional GNU Build System using the familiar
commands './configure && make && sudo make install' for building and installing
software.

*** Simple Project Structure
#+BEGINE file
.
├── AUTHORS          # a list of authors and emails - must
├── COPYING          # AGPL or GPL
├── COPYING.LESSER   # LGPL
├── HACKING          # a note for how to hack the program
├── HISTORY          # a history file
├── INSTALL          # installation instructions
├── NEWS             # release news - must
├── README           # readme file - must
├── THANKS           # thank you file
├── TODO             # a note for future tasks
├── ChangeLog        # a list of commit log - must
├── Makefile.am      # Automake file used to generate the Makefile.in which configure will configure. - yes
├── autogen.sh       # automatic bootstrap all, alternative to bootstrap file
├── ballandpaddle.desktop        # desktop file for the project - yes
├── ballandpaddle.160x160.xpm    # desktop icon file for the project - yes
├── bin              # final bin output folder - missing
├── build-aux        # a folder for build the program: doc, tests, etc - missing
│       └─ pre-inst-env.in  # a shell script which set up environment variables to be able to use your code before installing it.
├── bootstrap        # a simple shell script which a developer can regenerate all the GNU Build System files. - missing
├── configure.ac     # a template file which Autoconf uses to generate the familiar configure script. - yes
├── doc              # final doc output folder - yes
├── images           # project image folder - yes
├── levels           # project level folder - yes
├── levelsets        # project levelsets file - yes
├── m4               # a folder for m4 macros - yes
│   └── guile.m4    # a recent copy of Guile's m4 macros
│   └── aclocal.m4  # a possible copy of Guile's m4 macros
├── po               # a folder for project po files  - yes
├── src              # a folder for source code files - yes
│   └─ skeleton.scm  # some initial source code file - Guile module (skeleton)
├── src/skeleton
│     └─ hello.scm   # some initial source code file - (skeleton hello) module
└── tests            # a folder for test files - missing
       └─ base.scm    # some basic test caseshello.scm
#+END_SRC

*** Bootstrapping the Project
#+BEGIN_SRC shell
./bootstrap
#+END_SRC
It just calls autoreconf, which uses Autoconf and Automake, to generate the
configure script from configure.ac and Makefile.in file from Makefile.am. The
bootstrap script is sometimes also named autogen.sh in projects but seems to no
longer be preferred.

*** Generating the Project Makefile
#+BEGIN_SRC shell
automake  # use Makefile.am and guile.am to generate Makefile.in
#+END_SRC

**** top targets
bin_PROGRAMS - programs that should be compiled and installed.
bin_SCRIPTS - programs that should be installed but not compiled
man_MANS - man pages that should be installed.
lib_LTLIBRARIES - Libraries that should be built using libtool.
noinst_PROGRAMS - Programs that should be compiled but not installed.

**** source files of top targets
     One can specify the source files for each super target by typing the base
     name of the package (with _'s instead of -'s etc) followed by _SOURCES
     followed by an equal sign and a list of the sources.
#+BEGIN_SRC EXAMPLE
lib_LTLIBRARIES = libpthread_rwlock_fcfs.la
libpthread_rwlock_fcfs_la_SOURCES = rwlock.c queue.c
#+END_SRC

**** EXTRA_DIST
     The EXTRA_DIST argument holds a list of all the files that are part of the
     package, but are not installed by default and were not specified in any
     other way, e.g. a HACKING file.

**** Plain Rules just as in a Makefile
     One can use the meta-variables $< as the source and $@ as the target.

**** Sub-Directory with a Different Configuration
     To have a directory with its own separate configure script, first put it in
     the SUBDIRS variable of Makefile.am. Then use the AC_CONFIG_SUBDIRS macro
     in configure.in to configure them as separate sub-directories.

**** AC_OUTPUT
     If you have a custom mylibrary-config script, you need to put it in
     AC_OUTPUT. configure would expect a mylibrary-config.in file to be present.

*** Generating the Configure Script
#+BEGIN_SRC shell
autoreconf  # use configure.ac to generete configure script
#+END_SRC

*** GNU Guile Project Source Files
#+BEGIN_SRC shell
./configure  # configure Makefile.in, pre-inst-env.in
make         # compile source code
#+END_SRC

*** Distributing the Project
#+BEGIN_SRC shell
make dist  # generate a tar.gz file of the project
#+END_SRC
