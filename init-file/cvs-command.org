_*_ mode:org-mode _*
_
* CVS command for www.gnu.org project
#+BEGIN_SRC shell
cvs -z3 -d:ext:wxie@cvs.savannah.gnu.org:/web/www co www
cvs -z3 -d:ext:wxie@cvs.savannah.gnu.org:/web/www commit -m "..." file-name
cvs -z3 -d:ext:wxie@cvs.savannah.gnu.org:/web/www update file-name
#+END_SRC

* CVS command for www-zh-cn CTT project
#+BEGIN_SRC shell
# articles
cvs -z3 -d:ext:wxie@cvs.savannah.gnu.org:/cvsroot/www-zh-cn co <modulename>
cvs -z3 -d:ext:wxie@cvs.savannah.gnu.org:/cvsroot/www-zh-cn commit -m "update translation" home.zh-cn.po
cvs -z3 -d:ext:wxie@cvs.savannah.gnu.org:/cvsroot/www-zh-cn update home.zh-cn.po

# webpages
cvs -z3 -d:ext:wxie@cvs.savannah.gnu.org:/webcvs/www-zh-cn co www-zh-cn
#+END_SRC
