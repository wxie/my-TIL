;; -*- mode: Emacs-lisp; fill-column: 75; comment-column: 50; coding: utf-8 -*-
;;; ERC configuration
(use-package erc
  :commands (erc my-erc-start-or-switch)
  :config
  (setq erc-nick my-irc-nick)
  ;; Load authentication info from an external source.  Put sensitive
  ;; passwords and the like here:
  ;; (load "~/.emacs.d/.erc-auth")

  ;; This is an example of how to make a new command.  Type "/uptime" to
  ;; use it.
  (defun erc-cmd-UPTIME (&rest ignore)
    "Display the uptime of the system, as well as some load-related
     stuff, to the current ERC buffer."
    (let ((uname-output
           (replace-regexp-in-string
            ", load average: " "] {Load average} ["
            ;; Collapse spaces, remove
            (replace-regexp-in-string
             " +" " "
             ;; Remove beginning and trailing whitespace
             (replace-regexp-in-string
              "^ +\\|[ \n]+$" ""
              (shell-command-to-string "uptime"))))))
      (erc-send-message
       (concat "{Uptime} [" uname-output "]"))))

  ;; This causes ERC to connect to the Freenode network upon hitting
  ;; C-c e f.  Replace MYNICK with your IRC nick.
  (global-set-key "\C-cef" (lambda () (interactive)
                             (erc :server "irc.libera.chat" :port "6667"
                                  :nick erc-nick)))

  ;; This causes ERC to connect to the IRC server on your own machine (if
  ;; you have one) upon hitting C-c e b.  Replace MYNICK with your IRC
  ;; nick.  Often, people like to run bitlbee (http://bitlbee.org/) as an
  ;; AIM/Jabber/MSN to IRC gateway, so that they can use ERC to chat with
  ;; people on those networks.
  (global-set-key "\C-ceb" (lambda () (interactive)
                             (erc :server "localhost" :port "6667"
                                  :nick erc-nick)))

;;; Options

  ;; Join the #emacs and #erc channels whenever connecting to Libera.chat.
  (setq erc-autojoin-channels-alist '((".*\\.libera\\.chat" "#guix" "#guile"
                                       "#libreplanet-mod-private")))
  (erc-autojoin-mode t)

  ;; spell checking
  (erc-spelling-mode 1)

  ;; Rename server buffers to reflect the current network name instead
  ;; of SERVER:PORT (e.g., "freenode" instead of "irc.freenode.net:6667").
  ;; This is useful when using a bouncer like ZNC where you have multiple
  ;; connections to the same server.
  (setq erc-rename-buffers t)

  ;; Interpret mIRC-style color commands in IRC chats
  (setq erc-interpret-mirc-color t)

  ;; fallback to auth-source
  (setq erc-prompt-for-password nil)

  ;; Kill buffers for channels after /part
  (setq erc-kill-buffer-on-part t)
  ;; Kill buffers for private queries after quitting the server
  (setq erc-kill-queries-on-quit t)
  ;; Kill buffers for server messages after quitting the server
  (setq erc-kill-server-buffer-on-quit t)

  ;; open query buffers in the current window
  (setq erc-query-display 'buffer)

  (setq erc-auto-reconnect nil)

  (erc-track-mode t)
  (setq erc-track-exclude-types '("JOIN" "NICK" "PART" "QUIT" "MODE"
                                  "324" "329" "332" "333" "353" "477")))
