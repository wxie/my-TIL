;; prioritize non-byte-compiled source files to prevent the use of stale byte-code
(setq load-prefer-newer noninteractive)

