# Setups home environment profile
if [ -f ~/.profile ]; then source ~/.profile; fi

# Honor system-wide environment variables
if [[ -e /etc/profile ]]; then
    source /etc/profile
fi
