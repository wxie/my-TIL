;;; GNUS init file: ~/.gnus -*- mode: Emacs-Lisp; coding: utf-8 -*-

;; personal information
(setq user-full-name "Wensheng Xie"
      user-mail-address "xiewensheng@hotmail.com")

;; Send email through SMTP
(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-default-smtp-server "smtp-mail.outlook.com"
      smtpmail-smtp-service 587
      smtpmail-local-domain "G400")

(require 'nnir)

;; Please note mail folders in `gnus-select-method' have NO prefix like "nnimap+hotmail:" or "nnimap+safe-mail:"
(setq gnus-select-method '(nnnil "")) ; no primary server
;; (setq gnus-select-method '(nntp "nntp.aioe.org"))     ; nntp news server
;; (setq gnus-select-method '(nntp "newsfeed.aioe.org")) ; nntp news server

;; the setup for Microsoft Hotmail
(add-to-list 'gnus-secondary-select-methods
             '(nnimap "hotmail"
                      (nnimap-address "imap-mail.outlook.com")
                      (nnimap-server-port 993)
                      (nnimap-stream ssl)
                      (nnimap-streaming t)
                      (nnir-search-engine imap)
                      (nnmail-expiry-wait 90)))

;; the setup for Safe-mail.net
(add-to-list 'gnus-secondary-select-methods
             '(nnimap "safe-mail"
                      (nnimap-address "pop.safe-mail.net")
                      (nnimap-server-port 993)
                      (nnimap-stream ssl)
                      (nnimap-streaming t)
                      (nnir-search-engine imap)
                      (nnmail-expiry-wait 90)))

(setq gnus-thread-sort-functions
      '(gnus-thread-sort-by-most-recent-date
        (not gnus-thread-sort-by-number)))

; NO 'passive
(setq gnus-use-cache t)

;; {{ press "o" to view all groups
(defun wxie/gnus-group-list-subscribed-groups ()
  "List all subscribed groups with or without un-read messages"
  (interactive)
  (gnus-group-list-all-groups 5))

(define-key gnus-group-mode-map
  ;; list all the subscribed groups even they contain zero un-read messages
  (kbd "o") 'wxie/gnus-group-list-subscribed-groups)
;; }}

;; Fetch only part of the article if we can.
;; I saw this in someone's .gnus
(setq gnus-read-active-file 'some)

;; Tree view for groups.
(add-hook 'gnus-group-mode-hook 'gnus-topic-mode)

;; Threads!  I hate reading un-threaded email -- especially mailing
;; lists.  This helps a ton!
(setq gnus-summary-thread-gathering-function 'gnus-gather-threads-by-subject)

;; Also, I prefer to see only the top level message.  If a message has
;; several replies or is part of a thread, only show the first message.
;; `gnus-thread-ignore-subject' will ignore the subject and
;; look at 'In-Reply-To:' and 'References:' headers.
(setq gnus-thread-hide-subtree t)
(setq gnus-thread-ignore-subject t)

;; http://www.gnu.org/software/emacs/manual/html_node/gnus/_005b9_002e2_005d.html
(setq gnus-use-correct-string-widths nil)

;; Sample on how to organize mail folders.
;; It's dependent on `gnus-topic-mode'.
(eval-after-load 'gnus-topic
  '(progn
     (setq gnus-message-archive-group '((format-time-string "sent.%Y")))
     (setq gnus-server-alist '(("archive" nnfolder "archive" (nnfolder-directory "~/Mail/archive")
                                (nnfolder-active-file "~/Mail/archive/active")
                                (nnfolder-get-new-mail nil)
                                (nnfolder-inhibit-expiry t))))

     ;; "Gnus" is the root folder, and there are three mail accounts, "misc", "hotmail", "safe-mail"
     (setq gnus-topic-topology '(("Gnus" visible)
                                 (("misc" visible))
                                 (("hotmail" visible nil nil))
                                 (("safe-mail" visible nil nil))))

     ;; each topic corresponds to a public imap folder
     (setq gnus-topic-alist '(("hotmail" ; the key of topic
                               "nnimap+hotmail:Inbox"
                               "nnimap+hotmail:Drafts"
                               "nnimap+hotmail:Sent"
                               "nnimap+hotmail:Junk"
                               "nnimap+hotmail:Deleted")
                              ("safe-mail" ; the key of topic
                               "nnimap+safe-mail:INBOX"
                               "nnimap+safe-mail:[Safe-Mail]/Sent Mail"
                               "nnimap+safe-mail:[Safe-Mail]/Trash"
                               "nnimap+safe-mail:Drafts")
                              ("misc" ; the key of topic
                               "nnfolder+archive:sent.2020"
                               "nnfolder+archive:sent.2021"
                               "nndraft:drafts")
                              ("Gnus")))

     ;; see latest 200 mails in topic hen press Enter on any group
     (gnus-topic-set-parameters "safe-mail" '((display . 200)))
     (gnus-topic-set-parameters "hotmail" '((display . 200)))))
