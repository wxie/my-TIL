#
# ~/.bash_profile
#
# Honor per-interactive-shell startup file
# if [ -f ~/.bashrc ]; then . ~/.bashrc; fi
[[ -f ~/.bashrc ]] && . ~/.bashrc

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
    exec startx;
    # xinit -- vt01;
fi
