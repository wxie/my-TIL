;;; EMACS init file: ~/.emacs.d/init.el -*- mode: Emacs-Lisp -*-

;; When Emacs is started, it normally tries to load a Lisp program from an initialization file, or init file for short. This file, if it exists, specifies how to initialize Emacs for you. Emacs looks for your init file using the filenames ~/.emacs, ~/.emacs.el, or ~/.emacs.d/init.el; you can choose to use any one of these three names. Here, ~/ stands for your home directory.

;; If .emacs is not found inside ~/ (nor .emacs.el), Emacs looks for ~/.emacs.d/init.el (which, like ~/.emacs.el, can be byte-compiled).

;; use ~/.emacs.d/init-emacs.org as an org-style init file

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(message "Start of init...")

(package-initialize)

(require 'org)

(org-babel-load-file
 (expand-file-name "init-emacs.org" user-emacs-directory))

;; (load-file "~/.emacs.d/init-emacs.el")

(message "End of init")

;;; init.el ends here
