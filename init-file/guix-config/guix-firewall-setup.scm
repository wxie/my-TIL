;;;; [[https://forum.systemcrafters.net/t/whats-the-best-way-to-setup-a-firewall-in-guix/1474]]

;;; add the following service in config.scm
(service nftables-service-type
         (nftables-configuration
          (ruleset (plain-file "nftables.conf"
                               "\
# A simple and safe firewall (based on %default-nftables-ruleset)
define pub_iface = \"enp11s0\"
define wg_iface = \"wg0\"
define wg_port = 51820
define ssh_port = 22
define guix_publish_port = 8080

table inet filter {
  chain input {
    type filter hook input priority 0; policy drop;

    # early drop of invalid connections
    ct state invalid drop

    # allow established/related connections
    ct state { established, related } accept

    # allow from loopback
    iif lo accept
    # drop connections to lo not coming from lo
    iif != lo ip daddr 127.0.0.1/8 drop
    iif != lo ip6 daddr ::1/128 drop

    # allow icmp
    ip protocol icmp accept
    ip6 nexthdr icmpv6 accept

    # allow ssh
    tcp dport $ssh_port accept

    # allow guix publish
    tcp dport $guix_publish_port accept

    # allow wireguard port
    udp dport $wg_port accept

    # reject everything else
    reject with icmpx type port-unreachable
  }
  chain output {
    type filter hook output priority 0; policy accept;
  }
}
"))))
