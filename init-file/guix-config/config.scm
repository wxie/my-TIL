;; This is an operating system configuration generated
;; by the graphical installer.

;; modified by wxie
(use-modules (gnu) (gnu system nss))
(use-service-modules desktop networking ssh)
(use-package-modules certs emacs emacs-xyz xorg)

(operating-system
  (locale "en_US.utf8")
  (timezone "Asia/Shanghai")
  (host-name "guix")
  
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (targets '("/dev/sda"))))

  (swap-devices
   (list
    (swap-space
     (target "/dev/sda1"))))
  
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device
               (uuid "ac551a60-308c-4a49-b44c-cb28a99393da"
                     'ext4))
             (type "ext4"))
           %base-file-systems))
           
  (users (cons* (user-account
                  (name "wxie")
                  (comment "Wensheng XIE")
                  (group "users")
                  (home-directory "/home/wxie")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
                
  ;; Add a bunch of window managers; we can choose one at
  ;; the log-in screen with F1.
  (packages (append (list
                     ;; window managers
                     emacs emacs-exwm emacs-desktop-environment
                     ;; terminal emulator
                     xterm)
                    %base-packages))

  ;; Use the "desktop" services, which include the X11
  ;; log-in service, networking with NetworkManager, and more.
  (services %desktop-services)

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
