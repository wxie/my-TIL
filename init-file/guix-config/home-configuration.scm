;; Diese Datei mit Ihrer Persönlichen Umgebung kann an 'guix home reconfigure'
;; übergeben werden, um den Inhalt Ihres Profils nachzubilden. Sie ist
;; "symbolisch", gibt also nur die Namen der Pakete an. Um genau das gleiche
;; Profil herauszubekommen, müssen Sie auch die verwendeten Kanäle nachbilden,
;; wie "guix describe" sie anzeigt. Siehe den Abschnitt "Guix nachbilden"
;; im Handbuch.

(use-modules
  (gnu home)
  (gnu packages)
  (gnu services)
  (guix gexp)
  (gnu home services shells))

(home-environment
  (packages
    (map (compose list specification->package+output)
         (list "shepherd"
               "nyxt"
               "vlc"
               "ibus-libpinyin"
               "ibus-rime"
               "ibus"
               "emacs"
               "heimdall"
               "git"
               "fastboot"
               "openssh"
               "graphviz"
               "gnupg"
               "node"
               "mkbootimg"
               "python-pip"
               "emacs-guix"
               "emacs-geiser"
               "python"
               "gdb"
               "fontconfig"
               "adb"
               "postgresql"
               "curl"
               "guile:debug"
               "guile"
               "sbcl"
               "artanis"
               "gettext"
               "tar"
               "wget"
               "automake"
               "librime"
               "autoconf"
               "gnutls"
               "nss-certs"
               "glibc:debug"
               "aspell-dict-en"
               "glibc-locales"
               "font-gnu-freefont"
               "pkg-config"
               "minicom"
               "font-wqy-zenhei"
               "help2man"
               "texinfo"
               "aspell-dict-de"
               "make"
               "aspell"
               "font-dejavu"
               "font-ghostscript"
               "guile-readline")))
  (services
    (list (service
            home-bash-service-type
            (home-bash-configuration
              (aliases
                '(("grep" . "grep --color=auto")
                  ("ll" . "ls -l")
                  ("ls" . "ls -p --color=auto")))
              (bashrc
                (list (local-file
                        "/home/wxie/src/guix-config/.bashrc"
                        "bashrc")))
              (bash-profile
                (list (local-file
                        "/home/wxie/src/guix-config/.bash_profile"
                        "bash_profile"))))))))
