#!/usr/bin/env bash

set -Eeuo pipefail

# script logic here: initialize the po file from pot file using compendium
echo "Usage: ./init-po-file.sh file-name-without-extension"
msgmerge --compendium compendium.wxie-2023.po -o temp.zh-cn.po /dev/null $1.pot

# update the first line of the new file
head -1 $1.pot > $1.zh-cn.po
sed -i "1s/LANGUAGE/Simplified Chinese/" $1.zh-cn.po
cat temp.zh-cn.po >> $1.zh-cn.po

# clean up
rm temp.zh-cn.po $1.pot
