# -**- mode: org; fill-column: 75; comment-column: 50; coding: utf-8 -**-
# org-style ~/.emacs.d/init-emacs.org file used by ~/.emacs.d/init.el

#+STARTUP: overview indent inlineimages logdrawer
#+TITLE:       org-mode init file for emacs
#+AUTHOR:      Wensheng Xie
#+LANGUAGE:    en
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:t -:t f:t *:t <:t
#+COLUMNS: %25ITEM %TODO %3PRIORITY %TAGS

* personal information
#+BEGIN_SRC emacs-lisp
(setq user-full-name "Wensheng Xie"
      user-mail-address "xiewensheng@hotmail.com")
(setq my-irc-nick "wxie")

(eval-when-compile (require 'cl-lib))
#+END_SRC

* package management
#+BEGIN_SRC emacs-lisp
  (require 'package)
  (setq package-enable-at-startup nil)
  (setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")))
  (add-to-list 'package-archives
               ;'("marmalade" . "https://marmalade-repo.org/packages/")
               '("melpa" . "https://melpa.org/packages/")
               '("org" . "https://orgmode.org/elpa/"))
  (if (version< emacs-version "27.1")
      (package-initialize))

  (unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))

  (eval-when-compile
    (require 'use-package))

  (setq use-package-verbose t)

  ;; Disable lazy loading in daemon mode
  (if (daemonp)
      (setq use-package-always-demand t))
#+END_SRC

* Shortcuts
;; Many emacs shortcuts start by C-x. Org-mode's shortcuts generaly start with C-c
** Emacs shortcuts
;; C-x C-e call make
;; C-x C-h get the list of emacs shortcuts
;; C-c C-h get the list of emacs shortcuts considering the mode you are currently using (e.g., C, Lisp, org, …)

** Org-mode shortcuts
;; <s + tab template for source bloc
;; C-c C-c execute the block
* emacs general customization
#+BEGIN_SRC emacs-lisp
  ;; Set garbage collection threshold.
  (setq gc-cons-threshold-original gc-cons-threshold)
  (setq gc-cons-threshold most-positive-fixnum)

  ;; Restore default values after initialization.
  (add-hook 'after-init-hook
            (lambda ()
              (setq gc-cons-threshold gc-cons-threshold-original)
              (makunbound 'gc-cons-threshold-original)))

  ;; UTF-8 please
  (setq locale-coding-system 'utf-8) ; pretty
  (set-terminal-coding-system 'utf-8) ; pretty
  (set-keyboard-coding-system 'utf-8) ; pretty
  (set-selection-coding-system 'utf-8) ; please
  (prefer-coding-system 'utf-8) ; with sugar on top

  ;;custom files
  (setq custom-file (expand-file-name "custom-settings.el" user-emacs-directory))
  (load custom-file 'noerror)

  ;; Turn off mouse interface early in startup to avoid momentary display
  (when window-system
    (menu-bar-mode -1)
    (tool-bar-mode -1)
    (scroll-bar-mode -1)
    (tooltip-mode -1)
    (display-time-mode 1))

  (setq display-time-24hr-format t)
  (setq display-time-day-and-date t)
  (setq inhibit-startup-message t)
  (setq initial-scratch-message "")
  (setq-default indent-tabs-mode nil)
  (setq-default indicate-empty-lines t)

  ;; Do not create backup files
  (setq make-backup-files nil)

  ;; Auto-Save in /tmp
  (savehist-mode 1)
  (setq backup-directory-alist
        `((".*" . ,temporary-file-directory)))
  (setq auto-save-file-name-transforms
        `((".*" ,temporary-file-directory t)))

  ;; Always follow symlinks
  (setq vc-follow-symlinks t)

  ;; Don't count two spaces after a period as the end of a sentence.
  ;; Just one space is needed.
  (setq tab-always-indent 'complete)
  (setq sentence-end-double-space nil)
  (setq-default fill-column 80)
  (add-hook 'before-save-hook '(lambda()
                                 (when (not (or (derived-mode-p 'markdown-mode)))
                                   (delete-trailing-whitespace))))

  ;; show either a file or a buffer name
  (setq frame-title-format
        '("" invocation-name " - "
          (:eval (if (buffer-file-name)
                     (abbreviate-file-name (buffer-file-name))
                   "%b"))))

  ;; delete the region when typing, just like as we expect nowadays.
  (delete-selection-mode t)
  (transient-mark-mode t)
  (show-paren-mode t)
  (column-number-mode t)
  (line-number-mode t)

  ;; auto fill mode
  (defun auto-fill-mode-on () (auto-fill-mode 1))
  (add-hook 'text-mode-hook 'auto-fill-mode-on)
  (add-hook 'emacs-lisp-mode 'auto-fill-mode-on)
  (add-hook 'tex-mode-hook 'auto-fill-mode-on)
  (add-hook 'latex-mode-hook 'auto-fill-mode-on)
  (add-hook 'org-mode-hook 'auto-fill-mode)

  ;; Turn off the blinking cursor
  (blink-cursor-mode -1)

  ;; Don't beep at me
  (setq visible-bell t)

  ;; These functions are useful. Activate them.
  (put 'downcase-region 'disabled nil)
  (put 'upcase-region 'disabled nil)
  (put 'narrow-to-region 'disabled nil)

  ;; Answering just 'y' or 'n' will do
  (defalias 'yes-or-no-p 'y-or-n-p)

#+END_SRC

* utilities
** spelling check
#+BEGIN_SRC emacs-lisp
(use-package flyspell
  :ensure t
  :defer 10
  :preface
  (cond
   ((executable-find "aspell")
    (setq ispell-program-name "aspell"))
   ((executable-find "hunspell")
    (setq ispell-program-name "hunspell")
    (setq ispell-dictionary "en_US"))
   (t
    (message "Neither aspell nor hunspell found"))))

(setq flyspell-sort-corrections nil)
(setq flyspell-issue-message-flag nil)

(defun flyspell-switch-dictionary()
  "Switch between German and English dictionaries"
  (interactive)
  (let* ((dic ispell-current-dictionary)
         (change (if (string= dic "deutsch") "english" "deutsch")))
    (ispell-change-dictionary change)
    (message "Dictionary switched from %s to %s" dic change)))

#+END_SRC
** PO mode facility
#+BEGIN_SRC emacs-lisp
;; PO-mode long lines
(defun wxie/setup-po-subedit ()
 (visual-line-mode 1)
 (auto-fill-mode -1))

(add-hook 'po-subedit-mode-hook 'wxie/setup-po-subedit)
(add-hook 'po-subedit-exit-hook 'auto-fill-mode-on)

;; doing M-x po-wrap while in PO mode will wrap all long lines
(defun po-wrap ()
  "Filter current po-mode buffer through `msgcat' tool to wrap all lines."
  (interactive)
  (if (eq major-mode 'po-mode)
      (let ((tmp-file (make-temp-file "po-wrap."))
	    (tmp-buf (generate-new-buffer "*temp*")))
	(unwind-protect
	    (progn
	      (write-region (point-min) (point-max) tmp-file nil 1)
	      (if (zerop
		   (call-process
		    "msgcat" nil tmp-buf t (shell-quote-argument tmp-file)))
		  (let ((saved (point))
			(inhibit-read-only t))
		    (delete-region (point-min) (point-max))
		    (insert-buffer tmp-buf)
		    (goto-char (min saved (point-max))))
		(with-current-buffer tmp-buf
		  (error (buffer-string)))))
	  (kill-buffer tmp-buf)
	  (delete-file tmp-file)))))

#+END_SRC
** Dired mode
#+BEGIN_SRC emacs-lisp
(put 'dired-find-alternate-file 'disabled nil)
(setq-default dired-listing-switches "-lh")
(setq dired-recursive-copies 'always)
(setq dired-recursive-deletes 'always)

#+END_SRC
* file/mode association
#+BEGIN_SRC emacs-lisp
(autoload 'po-mode "po-mode"
  "Major mode for translators to edit PO files" t)

(setq auto-mode-alist
      (append (mapcar 'purecopy
                      '(("\\.c$"   . c-mode)
                        ("\\.h$"   . c-mode)
                        ("\\.c.simp$" . c-mode)
                        ("\\.h.simp$" . c-mode)
                        ("\\.a$"   . c-mode)
                        ("\\.w$"   . cweb-mode)
                        ("\\.cc$"   . c++-mode)
                        ("\\.S$"   . asm-mode)
                        ("\\.s$"   . asm-mode)
                        ("\\.tex$" . LaTeX-mode)
                        ("\\.txi$" . Texinfo-mode)
                        ("\\.el$"  . emacs-lisp-mode)
                        ("[mM]akefile" . makefile-mode)
                        ("[mM]akefile.*" . makefile-mode)
                        ("\\.mak" . makefile-mode)
                        ("\\.po\\'\\|\\.po\\." . po-mode)
                        ("\\.cshrc" . sh-mode)
                        ("\\.html$" . html-mode)
                        ("\\.\\(org\\|org_archive\\|txt\\)$" . org-mode)))
              auto-mode-alist))

;; To use the right coding system automatically under Emacs 20 or newer,
;; also add:
(autoload 'po-find-file-coding-system "po-compat")
(modify-coding-system-alist 'file "\\.po\\'\\|\\.po\\."
                            'po-find-file-coding-system)

#+END_SRC

* programming language
** inferior lisp
#+BEGIN_SRC emacs-lisp
; (ql:quickload "quicklisp-slime-helper")
(load (expand-file-name "~/quicklisp/slime-helper.el"))
(setq slime-contribs '(slime-fancy)) ; almost everything
(setq inferior-lisp-program "sbcl")
#+END_SRC

** Guile and Geiser
#+BEGIN_SRC emacs-lisp
(setq geiser-guile-load-init-file-p t)
(use-package geiser
  :ensure t
  :hook (scheme-mode . geiser-mode--maybe-activate)
  :config
  (setq geiser-active-implementations '(guile))
  (setq geiser-mode-start-repl-p t)
  (setq geiser-repl-history-filename
        (expand-file-name "geiser_history" user-emacs-directory)))

(use-package guix
  :ensure t
  :defer 20
  :hook (scheme-mode . guix-devel-mode))
#+END_SRC

* org Mode
** Load org-mode and related functions
#+BEGIN_SRC emacs-lisp
    ;; load up Org-mode and Org-babel
    ;(require 'org-install)
    (require 'org-habit)
    (require 'ob-tangle)
    (require 'org-tempo)

    ;; ledger
  (if (file-exists-p (concat user-emacs-directory "extensions/ob-ledger/ob-ledger.el"))
      (add-to-list 'load-path "~/.emacs.d/extensions/ob-ledger"))  ; directory for ob-ledger.el file
  (require 'ob-ledger)  ; install ob-ledger.el manually

    ;; ;; Explicitly load required exporters
    (require 'ox-html)
    (require 'ox-latex)
    (require 'ox-ascii)

    ;; org code system
    (setq org-export-coding-system 'utf-8)
#+END_SRC

** Default directory
#+BEGIN_SRC emacs-lisp
(setq default-directory "~/work/")
(setq org-directory "~/note/")
(setq org-default-notes-file "~/note/capture/capture-wxie.org")
(setq org-agenda-files (list org-directory))

;; Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings, and org-protocol
(setq org-capture-templates
      (quote (("t" "todo" entry (file "~/note/capture/capture-wxie.org")
               "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
              ("r" "respond" entry (file "~/note/capture/capture-wxie.org")
               "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
              ("n" "note" entry (file "~/note/capture/capture-wxie.org")
               "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
              ("j" "Journal" entry (file+datetree "~/note/capture/diary.org")
               "* %?\n%U\n" :clock-in t :clock-resume t)
              ("w" "org-protocol" entry (file "~/note/capture/capture-wxie.org")
               "* TODO Review %c\n%U\n" :immediate-finish t)
              ("m" "Meeting" entry (file "~/note/capture/capture-wxie.org")
               "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
              ("p" "Phone call" entry (file "~/note/capture/capture-wxie.org")
               "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
              ("h" "Habit" entry (file "~/note/capture/capture-wxie.org")
               "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))
#+END_SRC

** Settings Cosmetics
#+BEGIN_SRC emacs-lisp
(use-package org
  ;:ensure org-plus-contrib
  :defer 10
  :bind (("C-c a" . org-agenda)
         ("C-c c" . org-capture)
         ("C-c l" . org-store-link)
         ("C-c C-," . org-insert-structure-template))
  :config
  (setq org-id-track-globally nil)      ; Do not store org IDs on disk.
  (setq org-hide-leading-stars t)
  (setq org-alphabetical-lists t)
  (setq org-catch-invisible-edits 'error)
  (setq org-src-fontify-natively t)  ; you want this to activate coloring in blocks
  (setq org-src-tab-acts-natively t) ; you want this to have completion in blocks
  (setq org-hide-emphasis-markers t) ; to hide the *,=, or / markers
  (setq org-pretty-entities t))      ; to have \alpha, \to and others display as utf8 http://orgmode.org/manual/Special-symbols.html
#+END_SRC

** TODOs and TAGs
#+BEGIN_SRC emacs-lisp
;; TODO keywords
(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
              (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING"))))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("NEXT" :foreground "blue" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("WAITING" :foreground "orange" :weight bold)
              ("HOLD" :foreground "magenta" :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold)
              ("MEETING" :foreground "forest green" :weight bold)
              ("PHONE" :foreground "forest green" :weight bold))))

(setq org-todo-state-tags-triggers
      (quote (("CANCELLED" ("CANCELLED" . t))
              ("WAITING" ("WAITING" . t))
              ("HOLD" ("WAITING") ("HOLD" . t))
              (done ("WAITING") ("HOLD"))
              ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
              ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
              ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

;; org todo workflow
(setq org-log-done 'time)
(setq org-log-into-drawer t)
(setq org-use-fast-todo-selection t)
(setq org-treat-S-cursor-todo-selection-as-state-change nil)

;; TAGS keyword
;; Tags with fast selection keys
(setq org-tag-alist (quote ((:startgroup)
                            ("@office" . ?o)
                            ("@home" . ?h)
                            ("@elsewhere" . ?e)
                            (:endgroup)
                            ("WAITING" . ?w)
                            ("HOLD" . ?H)
                            ("PERSONAL" . ?P)
                            ("WORK" . ?W)
                            ("ORG" . ?O)
                            ("NOTE" . ?n)
                            ("CANCELLED" . ?c)
                            ("FLAGGED" . ??))))

;; Allow setting single tags without the menu
(setq org-fast-tag-selection-single-key (quote expert))

;; For tag searches ignore tasks with scheduled and deadline dates
(setq org-agenda-tags-todo-honor-ignore-options t)
#+END_SRC

** Agenda
#+BEGIN_SRC emacs-lisp
(setq org-agenda-include-all-todo t)
(setq org-agenda-include-diary t)
(setq org-agenda-default-appointment-duration 60)
(setq org-agenda-compact-blocks t)
(setq org-agenda-span 'month)
(setq org-agenda-start-on-weekday nil)

;; Keep tasks with dates on the global todo lists
(setq org-agenda-todo-ignore-with-date nil)

;; Keep tasks with deadlines on the global todo lists
(setq org-agenda-todo-ignore-deadlines nil)

;; Keep tasks with scheduled dates on the global todo lists
(setq org-agenda-todo-ignore-scheduled nil)

;; Keep tasks with timestamps on the global todo lists
(setq org-agenda-todo-ignore-timestamp nil)

;; Remove completed deadline tasks from the agenda view
(setq org-agenda-skip-deadline-if-done t)

;; Remove completed scheduled tasks from the agenda view
(setq org-agenda-skip-scheduled-if-done t)

;; Remove completed items from search results
(setq org-agenda-skip-timestamp-if-done t)
#+END_SRC

** Babel
#+BEGIN_SRC emacs-lisp
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (js .t)
     (lisp . t)
     (makefile . t)
     (scheme . t)
     (shell . t)
     (ledger . t)
     (sql . t)))

  (setq org-src-preserve-indentation t)
#+END_SRC

* ERC
#+BEGIN_SRC emacs-lisp
(require 'erc)

;; erc-sasl
(if (file-exists-p (concat user-emacs-directory "extensions/erc-sasl/erc-sasl.el"))
    (use-package erc-sasl
      :load-path "extensions/erc-sasl/"
      :after erc
      :config
      (add-to-list 'erc-sasl-server-regexp-list "irc\\.libera\\.chat")
      (defun erc-login ()
        "Perform user authentication at the IRC server."
        (erc-log (format "login: nick: %s, user: %s %s %s :%s"
                         (erc-current-nick)
                         (user-login-name)
                         (or erc-system-name (system-name))
                         erc-session-server
                         erc-session-user-full-name))
        (if erc-session-password
            (erc-server-send (format "PASS %s" erc-session-password))
          (message "Logging in without password"))
        (when (and (featurep 'erc-sasl) (erc-sasl-use-sasl-p))
          (erc-server-send "CAP REQ :sasl"))
        (erc-server-send (format "NICK %s" (erc-current-nick)))
        (erc-server-send
         (format "USER %s %s %s :%s"
                 ;; hacked - S.B.
                 (if erc-anonymous-login erc-email-userid (user-login-name))
                 "0" "*"
                 erc-session-user-full-name))
        (erc-update-mode-line))))

(defun my-erc-start-or-switch ()
  "Connect to ERC, or switch to last active buffer"
  (interactive)
  (if (get-buffer "irc.libera.chat:6697")
      (erc-track-switch-buffer 1)
    (when (y-or-n-p "Start ERC? ")
      (erc-tls :server "irc.libera.chat" :port 6697
               :nick my-irc-nick
               :password "*IRC4wxie!"))))

;; The rest is in the file ~/.emacs.d/.ercrc.el

#+END_SRC

* Magit
#+BEGIN_SRC emacs-lisp
(use-package magit
  :ensure t
  :mode ("/\\(\
\\(\\(COMMIT\\|NOTES\\|PULLREQ\\|TAG\\)_EDIT\\|MERGE_\\|\\)MSG\
\\|\\(BRANCH\\|EDIT\\)_DESCRIPTION\\)\\'" . git-commit-mode)
  :bind (("C-x g" . magit-status)
         ("C-x M-g" . magit-dispatch)
         ("C-c M-g" . magit-file-dispatch)))

#+END_SRC
* Guix
#+begin_src emacs-lisp
;; Assuming the Guix checkout is in ~/work/guix.
(with-eval-after-load 'geiser-guile
  (add-to-list 'geiser-guile-load-path "~/work/guix"))

(with-eval-after-load 'yasnippet
  (add-to-list 'yas-snippet-dirs "~/work/guix/etc/snippets"))
#+end_src
* web browser: eww
#+begin_src elisp
(setq
 browse-url-browser-function 'eww-browse-url ; Use eww by default
 shr-use-fonts  nil                          ; No special fonts
 shr-use-colors nil                          ; No colours
 shr-indentation 2                           ; Left-side margin
 shr-width 70                                ; Fold text for comfiness
 eww-search-prefix "https://www.qwant.com/?q=" ; Use qwant
 url-privacy-level '(email agent cookies lastloc))

(when (fboundp 'eww)
  (defun xah-rename-eww-buffer ()
    "Rename `eww-mode' buffer so sites open in new page.
URL `http://ergoemacs.org/emacs/emacs_eww_web_browser.html'
Version 2017-11-10"
    (let (($title (plist-get eww-data :title)))
      (when (eq major-mode 'eww-mode )
        (if $title
            (rename-buffer (concat "eww " $title ) t)
          (rename-buffer "eww" t)))))

  (add-hook 'eww-after-render-hook 'xah-rename-eww-buffer))
#+end_src
* use EXWM
*** init setup
#+BEGIN_SRC emacs-lisp
(require 'exwm)

;; simpel system tray
  ; (require 'exwm-systemtray)
  ; (exwm-systemtray-enable)
(exwm-systemtray-mode 1)
(exwm-enable)

; (if (file-exists-p (concat user-emacs-directory "extensions/exwm-config/exwm-config.el"))
;     (add-to-list 'load-path "~/.emacs.d/extensions/exwm-config"))  ; directory for exwm-config.el file
; (require 'exwm-config) ; install exwm-config.el
; (exwm-config-example)

;; using xim input
(setenv "GTK_IM_MODULE" "xim")
(setenv "QT_IM_MODULE" "xim")
(setenv "XMODIFIERS" "@im=exwm-xim")
(setenv "CLUTTER_IM_MODULE" "xim")
  ; (require 'exwm-xim)
  ; (exwm-xim-enable)
(exwm-xim-mode 1)
(push ?\C-\\ exwm-input-prefix-keys)   ;; 使用Ctrl + \ 切换输入法
#+END_SRC
*** key bindings
| key         | description                        |
|-------------+------------------------------------|
| s-N         | change workstation to N            |
| s-w         | select workstation                 |
| s-r         | switch to line mode                |
| C-c C-f     | swtich to full screen              |
| C-c C-h     | hide floating X window             |
| C-c C-k     | switch to char mode                |
| C-c C-m     | move window to another workstation |
| C-c C-q     | send a singel key to X window      |
| C-c C-t C-m | toggle mode line                   |
| C-c C-t C-f | toggle floating and tilt           |
| C-\         | switch input method                |
|-------------+------------------------------------|

* pyim
#+BEGIN_SRC emacs-lisp
  ;; M-x package-install RET pyim RET
  ;; pyim 中文输入法
  (require 'pyim)
  (require 'pyim-basedict) ; 拼音词库设置，五笔用户 *不需要* 此行设置
  (pyim-basedict-enable)
  (setq default-input-method "pyim")
  (setq pyim-page-tooltip 'posframe)
  ;; (setq pyim-default-scheme 'wubi)    ;; 五笔
  ;; (setq pyim-default-scheme 'pyim-shuangpin) ;;双拼
  (setq pyim-default-scheme 'quanpin) ;; 全拼
  (global-set-key (kbd "C-\\") 'toggle-input-method) ;; 使用 Ctrl + \ 作为切换输入法状态的按键
#+END_SRC

* EMMS
[[http://www.gnu.org/software/emms/manual/][Emms is the Emacs Multi-Media System.]]
#+BEGIN_SRC emacs-lisp
  (require 'emms-setup)
  (emms-minimalistic)     ; (emms-all)

  (setq-default
;;   emms-source-file-default-directory "~/Videos/"

   emms-player-list '(emms-player-vlc)
   emms-info-functions '(emms-info-native))
#+END_SRC

* eradio
eradio is a simple Internet radio player for Emacs.
#+BEGIN_SRC emacs-lisp
(setq eradio-player '("vlc" "--no-video" "-I" "rc"))

(setq eradio-channels '(("def con - soma fm" . "https://somafm.com/defcon256.pls")          ;; electronica with defcon-speaker bumpers
                        ("metal - soma fm"   . "https://somafm.com/metal130.pls")           ;; \m/
                        ("cyberia - lainon"  . "https://lainon.life/radio/cyberia.ogg.m3u") ;; cyberpunk-esque electronica
                        ("cafe - lainon"     . "https://lainon.life/radio/cafe.ogg.m3u")    ;; boring ambient, but with lain
                        ("lush - soma fm"    . "https://somafm.com/lush.pls")))             ;; Sensuous and mellow female vocals
#+END_SRC
