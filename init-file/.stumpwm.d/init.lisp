   ;;; stumpwm init file: ~/.stumpwm.d/init.lisp -*- mode: Lisp -*-


(setf *mouse-focus-policy* :click)

(setf *window-format* "%m%n%s%c")
(setf *screen-mode-line-format* (list "[^B%n^b] %W^>%d"))

(setf *time-modeline-string* "%a %b %e %k:%M")

(setf *mode-line-timeout* 2)

(enable-mode-line (current-screen) (current-head) t)
