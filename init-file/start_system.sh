#!/usr/bin/bash

# BASH script for k42a network startup
set -Eeuxo pipefail

# disable network service from boot, only need once
# systemctl disable systemd-networkd.service
# systemctl disable systemd-resolved.service

# stop network service, in case not disabled
# systemctl stop systemd-networkd.service

# bringup interfaces
ip link
ip link set dev enp2s0 up
ip link set dev wlp3s0 up

# connect to wireless SSID
echo "Please choose a WiFi hotspot: "
echo "0 - CMCC-vh24-5G"
echo "1 - Biz_557"
echo "2 - ChinaNet-hSMH"
echo "3 - ChinaNet-TimeFly"
echo "4 - ChinaNet-TimeFly-5G"
echo "5 - OCNMH_3767A0"
echo "6 - Redmi"
echo "7 - ChinaNet-xtgy2018"
echo "8 - Project West Lake"
echo "9 - CMCC-vh24"
echo "a - Xiaomi_4995_5G"
echo "b - Xiaomi_4995"

echo "q - skip this step"

read hotspot
case $hotspot in
    0) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "CMCC-vh24-5G" "2f2eax3s");;
    1) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "Biz_557" "20190701");;
    2) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "ChinaNet-hSMH" "i0gbh0L7M4");;
    3) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "ChinaNet-TimeFly" "Timefly@2021");;
    4) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "ChinaNet-TimeFly-5G" "Timefly@2021");;
    5) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "OCNMH_3767A0" "Ask4Help");;
    6) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "Redmi" "Redmi-2022");;
    7) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "ChinaNet-xtgy2018" "xtgy2018");;
    8) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "Project West Lake" "projwestlake");;
    9) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "CMCC-vh24" "2f2eax3s");;
    a) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "Xiaomi_4995_5G" "ssf123456");;
    b) wpa_supplicant -B -i wlp3s0 -c <(wpa_passphrase "Xiaomi_4995" "ssf123456");;

    *) echo "No hotspot selected. No wireless network.";;
esac

# connect to SSID use wpa_cli
# wpa_supplicant -B -i wlp14s0 -c /etc/wpa_supplicant/wpa_supplicant.conf
# wpa_cli -> scan -> scan_results -> add_network -> set_network 0 ssid "xxx" -> set_network 0 psk "pass" -> enable_network 0 -> save_config -> quit

# restart network service
systemctl start systemd-networkd.service

# start avahi daemon
systemctl start avahi-daemon.service

# check status
ip link

# start cups service
systemctl start cups.service

# update arch system
sleep 5
while true; do
    read -p "Do you want to update the system? (y/n)" yn
    case $yn in
	[yY]) pacman -Syyuu;
	      pacman -Qdtq | pacman -Rs -;
	      break;;
	[nN]) echo "No update this time.";
	      exit;;
	
	*)    echo "Invalid response";;
    esac
done


